#include "Users/Administrator.h"

#include <iostream>

Administrator::Administrator()
	: User()
{
}
Administrator::~Administrator()
{
}

void Administrator::View()
{
	std::cout << "[Administrator] Viewing data" << std::endl;
}

void Administrator::Write()
{
	std::cout << "[Administrator] Writing data" << std::endl;
}

