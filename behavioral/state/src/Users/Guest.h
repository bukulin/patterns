#ifndef __GUEST_H__
#define __GUEST_H__ 1

#include "Users/User.h"

class Guest : public User
{
public:
	Guest();
	virtual ~Guest();

public:
	virtual void View();
	virtual void Write();
};

#endif
