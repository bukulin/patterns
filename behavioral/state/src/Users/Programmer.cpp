#include "Users/Programmer.h"

#include <iostream>

Programmer::Programmer()
	: User()
{
}
Programmer::~Programmer()
{
}

void Programmer::View()
{
	std::cout << "[Programmer] Viewing data" << std::endl;
}

void Programmer::Write()
{
	std::cout << "[Programmer] Cannot write data" << std::endl;
}

