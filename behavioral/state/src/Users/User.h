#ifndef __USER_H__
#define __USER_H__ 1

class User
{
protected:
	User();
public:
	virtual ~User();

public:
	virtual void View() = 0;
	virtual void Write() = 0;
};

#endif
