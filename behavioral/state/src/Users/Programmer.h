#ifndef __PROGRAMMER_H__
#define __PROGRAMMER_H__ 1

#include "Users/User.h"

class Programmer : public User
{
public:
	Programmer();
	virtual ~Programmer();

public:
	virtual void View();
	virtual void Write();
};

#endif
