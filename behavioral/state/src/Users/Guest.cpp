#include "Users/Guest.h"

#include <iostream>

Guest::Guest()
	: User()
{
}
Guest::~Guest()
{
}

void Guest::View()
{
	std::cout << "[Guest] Cannot view data" << std::endl;
}

void Guest::Write()
{
	std::cout << "[Guest] Cannot write data" << std::endl;
}

