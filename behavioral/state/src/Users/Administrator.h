#ifndef __ADMINISTRATOR_H__
#define __ADMINISTRATOR_H__ 1

#include "Users/User.h"

class Administrator : public User
{
public:
	Administrator();
	virtual ~Administrator();

public:
	virtual void View();
	virtual void Write();
};

#endif
