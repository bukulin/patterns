#ifndef DbTable_h
#define DbTable_h 1

class User;

class DbTable
{
public:
	DbTable();
	~DbTable();

public:
	void SetUser(User* user);
	void DisplayState();
	void View();
	void Write();

private:
	User* m_User;
};

#endif
