#include "DbTable/DbTable.h"

#include "Users/User.h"

#include <iostream>
#include <typeinfo>

DbTable::DbTable()
	: m_User(NULL)
{
}
DbTable::~DbTable()
{
}

void DbTable::SetUser(User* aUser)
{
	m_User = aUser;
}

void DbTable::DisplayState()
{
	std::cout << "Current user is: "
		  << typeid(*m_User).name()
		  << std::endl;
}

void DbTable::View()
{
	m_User->View();
}
void DbTable::Write()
{
	m_User->Write();
}

