#include "DbTable/DbTable.h"
#include "Users/Administrator.h"
#include "Users/Guest.h"
#include "Users/Programmer.h"

void UseTable(DbTable* dbTable, User* user)
{
	dbTable->SetUser(user);
	dbTable->DisplayState();
	dbTable->View();
	dbTable->Write();
}

int main()
{
	DbTable dbTable;
	Administrator administrator;
	Guest guest;
	Programmer programmer;
	UseTable(&dbTable, &administrator);
	UseTable(&dbTable, &guest);
	UseTable(&dbTable, &programmer);

	return 0;
}
