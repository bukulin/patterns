#include "context/Context.h"
#include "strategies/Strategy.h"
#include "strategies/ConcreteStrategyAdd.h"
#include "strategies/ConcreteStrategySub.h"
#include "strategies/ConcreteStrategyMultiply.h"

#include <iostream>

int main()
{
	Context* context = NULL;
	Strategy* strategy = NULL;

	strategy = new ConcreteStrategyAdd();
	context = new Context( strategy );
	int resultA = context->executeStrategy(3, 4);
	delete context;
	context = NULL;
	delete strategy;
	strategy = NULL;
	std::cout << "Add: " << resultA << std::endl;

	strategy = new ConcreteStrategySub();
	context = new Context( strategy );
	int resultB = context->executeStrategy(3, 4);
	delete context;
	context = NULL;
	delete strategy;
	strategy = NULL;
	std::cout << "Sub: " << resultB << std::endl;

	strategy = new ConcreteStrategyMultiply();
	context = new Context( strategy );
	int resultC = context->executeStrategy(3, 4);
	delete context;
	context = NULL;
	delete strategy;
	strategy = NULL;
	std::cout << "Mul: " << resultC << std::endl;

	return 0;
}
