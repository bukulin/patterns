#ifndef _CONTEXT_H
#define _CONTEXT_H

class Strategy;

class Context
{
public:
	Context(Strategy* strategy);
	~Context();

public:
	int executeStrategy(int a, int b);

private:
	Strategy* strategy;
};

#endif
