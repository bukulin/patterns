#include "context/Context.h"

#include "strategies/Strategy.h"

Context::Context(Strategy* aStrategy)
	: strategy( aStrategy )
{}
Context::~Context()
{}

int Context::executeStrategy(int a, int b)
{
	return strategy->execute(a, b);
}

