#ifndef _STRATEGY_H
#define _STRATEGY_H

class Strategy
{
protected:
	Strategy();
public:
	virtual ~Strategy();

public:
	virtual int execute(int a, int b) = 0;
};

#endif
