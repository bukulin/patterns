#include "strategies/ConcreteStrategySub.h"

ConcreteStrategySub::ConcreteStrategySub()
	: Strategy()
{}
ConcreteStrategySub::~ConcreteStrategySub()
{}

int ConcreteStrategySub::execute(int a, int b)
{
	return a - b;
}
