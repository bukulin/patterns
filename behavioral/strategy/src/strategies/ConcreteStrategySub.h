#ifndef _CONCRETE_STRATEGY_SUB_H
#define _CONCRETE_STRATEGY_SUB_H

#include "strategies/Strategy.h"

class ConcreteStrategySub : public Strategy
{
public:
	ConcreteStrategySub();
	virtual ~ConcreteStrategySub();

public:
	virtual int execute(int a, int b);
};

#endif
