#ifndef _CONCRETE_STRATEGY_ADD_H
#define _CONCRETE_STRATEGY_ADD_H

#include "strategies/Strategy.h"

class ConcreteStrategyAdd : public Strategy
{
public:
	ConcreteStrategyAdd();
	virtual ~ConcreteStrategyAdd();

public:
	virtual int execute(int a, int b);
};

#endif
