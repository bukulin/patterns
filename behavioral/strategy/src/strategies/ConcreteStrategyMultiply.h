#ifndef _CONCRETE_STRATEGY_MULTIPLY_H
#define _CONCRETE_STRATEGY_MULTIPLY_H

#include "strategies/Strategy.h"

class ConcreteStrategyMultiply : public Strategy
{
public:
	ConcreteStrategyMultiply();
	virtual ~ConcreteStrategyMultiply();

public:
	virtual int execute(int a, int b);
};

#endif
