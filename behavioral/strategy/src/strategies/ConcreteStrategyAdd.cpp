#include "strategies/ConcreteStrategyAdd.h"

ConcreteStrategyAdd::ConcreteStrategyAdd()
	: Strategy()
{}
ConcreteStrategyAdd::~ConcreteStrategyAdd()
{}

int ConcreteStrategyAdd::execute(int a, int b)
{
	return a + b;
}
