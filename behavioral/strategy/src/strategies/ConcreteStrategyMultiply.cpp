#include "strategies/ConcreteStrategyMultiply.h"

ConcreteStrategyMultiply::ConcreteStrategyMultiply()
	: Strategy()
{}
ConcreteStrategyMultiply::~ConcreteStrategyMultiply()
{}

int ConcreteStrategyMultiply::execute(int a, int b)
{
	return a * b;
}
