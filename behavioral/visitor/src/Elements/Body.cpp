#include "Elements/Body.h"

#include "Visitors/CarElementVisitor.h"

Body::Body()
	: CarElement()
{}

Body::~Body()
{}

void Body::Accept(CarElementVisitor& visitor) const
{
	visitor.visit(*this);
}
