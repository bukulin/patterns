#ifndef Body_h
#define Body_h 1

#include "Elements/CarElement.h"

class Body: public CarElement
{
public:
	Body();
	~Body();

public:
	virtual void Accept(CarElementVisitor& visitor) const;
};

#endif
