#ifndef Car_h
#define Car_h 1

#include "Elements/CarElement.h"

#include <list>

class Car: public CarElement
{
public:
	Car();
	~Car();

public:
	virtual void Accept(CarElementVisitor& visitor) const;

private:
	std::list<CarElement*> elements;
};

#endif
