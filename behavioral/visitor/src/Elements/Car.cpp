#include "Elements/Car.h"

#include "Elements/Wheel.h"
#include "Elements/Engine.h"
#include "Elements/Body.h"

#include "Visitors/CarElementVisitor.h"

using namespace std;

Car::Car()
	: CarElement()
	, elements()
{
	elements.push_back( new Wheel("front left") );
	elements.push_back( new Wheel("front right") );
	elements.push_back( new Wheel("rear left") );
	elements.push_back( new Wheel("rear right") );
	elements.push_back( new Body() );
	elements.push_back( new Engine() );
}

Car::~Car()
{
	while(!elements.empty())
	{
		delete elements.back();
		elements.pop_back();
	}
}

void Car::Accept(CarElementVisitor& visitor) const
{
	for( list<CarElement*>::const_iterator each = elements.begin();
	     each != elements.end();
	     ++each )
	{
		(*each)->Accept(visitor);
	}
	visitor.visit(*this);
}
