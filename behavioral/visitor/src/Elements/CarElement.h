#ifndef CarElement_h
#define CarElement_h 1

class CarElementVisitor;

class CarElement
{
protected:
	CarElement();
public:
	virtual ~CarElement();

public:
	virtual void Accept(CarElementVisitor& visitor) const = 0;
};

#endif // CarElement_h
