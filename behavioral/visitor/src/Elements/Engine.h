#ifndef Engine_h
#define Engine_h 1

#include "Elements/CarElement.h"

class Engine: public CarElement
{
public:
	Engine();
	~Engine();

public:
	virtual void Accept(CarElementVisitor& visitor) const;
};

#endif
