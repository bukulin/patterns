#include "Elements/Wheel.h"

#include "Visitors/CarElementVisitor.h"

using namespace std;

Wheel::Wheel(const string& aName)
	: CarElement()
	, name(aName)
{}

Wheel::~Wheel()
{}

void Wheel::Accept(CarElementVisitor& visitor) const
{
	visitor.visit(*this);
}

const string& Wheel::GetName() const
{
	return name;
}
