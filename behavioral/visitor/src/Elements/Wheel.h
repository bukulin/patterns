#ifndef Wheel_h
#define Wheel_h 1

#include "Elements/CarElement.h"

#include <string>

class Wheel: public CarElement
{
public:
	Wheel(const std::string& name);
	~Wheel();

public:
	virtual void Accept(CarElementVisitor& visitor) const;

public:
	const std::string& GetName() const;

private:
	const std::string name;
};

#endif // Wheel_h
