#include "Elements/Engine.h"

#include "Visitors/CarElementVisitor.h"

Engine::Engine()
	: CarElement()
{}

Engine::~Engine()
{}

void Engine::Accept(CarElementVisitor& visitor) const
{
	visitor.visit(*this);
}
