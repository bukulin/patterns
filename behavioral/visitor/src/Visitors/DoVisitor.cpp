#include "Visitors/DoVisitor.h"

#include "Elements/Wheel.h"

#include <iostream>

using namespace std;

DoVisitor::DoVisitor()
	: CarElementVisitor()
{
}
DoVisitor::~DoVisitor()
{
}

void DoVisitor::visit(const Wheel& wheel)
{
	cout << "Kicking my "
	     << wheel.GetName()
	     << " wheel" << endl;
}
void DoVisitor::visit(const Engine& engine)
{
	cout << "Starting my engine" << endl;

}
void DoVisitor::visit(const Body& body)
{
	cout << "Moving my body" << endl;

}
void DoVisitor::visit(const Car& car)
{
	cout << "Starting my car" << endl;
}
