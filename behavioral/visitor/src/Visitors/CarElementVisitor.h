#ifndef CarElementVisitor_h
#define CarElementVisitor_h 1

class Wheel;
class Engine;
class Body;
class Car;

class CarElementVisitor
{
protected:
	CarElementVisitor();
public:
	virtual ~CarElementVisitor();

public:
	virtual void visit(const Wheel& wheel) = 0;
	virtual void visit(const Engine& engine) = 0;
	virtual void visit(const Body& body) = 0;
	virtual void visit(const Car& car) = 0;
};

#endif
