#ifndef DoVisitor_h
#define DoVisitor_h 1

#include "Visitors/CarElementVisitor.h"

class DoVisitor: public CarElementVisitor
{
public:
	DoVisitor();
	~DoVisitor();

public:
	virtual void visit(const Wheel& wheel);
	virtual void visit(const Engine& engine);
	virtual void visit(const Body& body);
	virtual void visit(const Car& car);
};

#endif
