#ifndef PrintVisitor_h
#define PrintVisitor_h 1

#include "Visitors/CarElementVisitor.h"

class PrintVisitor: public CarElementVisitor
{
public:
	PrintVisitor();
	~PrintVisitor();

public:
	virtual void visit(const Wheel& wheel);
	virtual void visit(const Engine& engine);
	virtual void visit(const Body& body);
	virtual void visit(const Car& car);
};

#endif
