#include "Visitors/PrintVisitor.h"

#include "Elements/Wheel.h"

#include <iostream>

using namespace std;

PrintVisitor::PrintVisitor()
	: CarElementVisitor()
{}

PrintVisitor::~PrintVisitor()
{}

void PrintVisitor::visit(const Wheel& wheel)
{
	cout << "Visiting "
	     << wheel.GetName()
	     << " wheel" << endl;
}
void PrintVisitor::visit(const Engine& engine)
{
	cout << "Visiting engine" << endl;
}
void PrintVisitor::visit(const Body& body)
{
	cout << "Visiting body" << endl;
}
void PrintVisitor::visit(const Car& car)
{
	cout << "Visiting car" << endl;
}


