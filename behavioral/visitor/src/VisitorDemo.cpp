#include "Elements/Car.h"

#include "Visitors/PrintVisitor.h"
#include "Visitors/DoVisitor.h"

int main(void)
{
	CarElement* car = new Car();

	CarElementVisitor* visitor = new PrintVisitor();
	car->Accept( *visitor );
	delete visitor;

	visitor = new DoVisitor();
	car->Accept( *visitor );
	delete visitor;

	delete car;

	return 0;
}
