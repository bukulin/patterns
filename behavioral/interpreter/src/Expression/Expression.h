#ifndef __EXPRESSION_H__
#define __EXPRESSION_H__ 1

#include <string>
#include <map>

class Expression
{
protected:
    Expression();
public:
    virtual ~Expression();

public:
    virtual int interpret(std::map< std::string, Expression* > variables) = 0;
};


#endif
