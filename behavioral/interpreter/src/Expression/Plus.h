#ifndef __PLUS_H__
#define __PLUS_H__ 1

#include "Expression/Expression.h"

class Plus : public Expression
{
public:
    Plus(Expression& left, Expression& right);
    virtual ~Plus();

public:
    virtual int interpret(std::map< std::string, Expression* > variables);

private:
    Expression& m_Left;
    Expression& m_Right;

};

#endif
