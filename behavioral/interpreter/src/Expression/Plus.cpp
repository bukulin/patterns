#include "Expression/Plus.h"

Plus::Plus(Expression& aLeft,
	   Expression& aRight)
    : Expression()
    , m_Left(aLeft)
    , m_Right(aRight)
{
}
Plus::~Plus()
{
}

int Plus::interpret(std::map< std::string, Expression* > variables)
{
    return m_Left.interpret(variables) + m_Right.interpret(variables);
}
