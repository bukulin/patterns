#ifndef __EVALUATOR_H__
#define __EVALUATOR_H__ 1

#include "Expression/Expression.h"

#include <vector>
#include <list>

class Evaluator : public Expression
{
public:
    Evaluator(const std::string& expression);
    virtual ~Evaluator();

public:
    virtual int interpret(std::map< std::string, Expression* > context);

private:
    static std::vector< std::string > tokenize(const std::string& expression);

private:
    Expression* m_SyntaxTree;
    std::list< Expression* > m_Expressions;
};


#endif
