#include "Expression/Minus.h"

Minus::Minus(Expression& aLeft,
	   Expression& aRight)
    : Expression()
    , m_Left(aLeft)
    , m_Right(aRight)
{
}
Minus::~Minus()
{
}

int Minus::interpret(std::map< std::string, Expression* > variables)
{
    return m_Left.interpret(variables) - m_Right.interpret(variables);
}
