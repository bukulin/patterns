#include "Expression/Number.h"

Number::Number(int aNumber)
    : Expression()
    , m_Number(aNumber)
{
}
Number::~Number()
{
}

int Number::interpret(std::map< std::string, Expression* > variables)
{
    return m_Number;
}
