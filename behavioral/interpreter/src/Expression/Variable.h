#ifndef __VARIABLE_H__
#define __VARIABLE_H__ 1

#include "Expression/Expression.h"

class Variable : public Expression
{
public:
    Variable(const std::string& name);
    virtual ~Variable();

public:
    virtual int interpret(std::map< std::string, Expression* > variables);

private:
    const std::string m_Name;
};

#endif
