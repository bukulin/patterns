#ifndef __NUMBER_H__
#define __NUMBER_H__ 1

#include "Expression/Expression.h"

class Number : public Expression
{
public:
    Number(int number);
    virtual ~Number();

public:
    virtual int interpret(std::map< std::string, Expression* > variables);

private:
    int m_Number;
};

#endif
