#include "Expression/Variable.h"

Variable::Variable(const std::string& aName)
    : Expression()
    , m_Name(aName)
{
}
Variable::~Variable()
{
}

int Variable::interpret(std::map< std::string, Expression* > variables)
{
    if( NULL == variables[m_Name] )
    {
	return 0;
    }

    return variables[m_Name]->interpret(variables);
}

