#include "Expression/Evaluator.h"

#include "Expression/Plus.h"
#include "Expression/Minus.h"
#include "Expression/Variable.h"

#include <list>
#include <sstream>
#include <iterator>

using namespace std;

Evaluator::Evaluator(const string& aExpression)
        : Expression()
        , m_SyntaxTree()
{
        list< Expression* > expressionStack;
        vector< string > tokens = tokenize(aExpression);
        for(vector< string >::const_iterator it = tokens.begin();
            it != tokens.end();
            ++it)
        {
                if( (*it) == "+" )
                {
                        Expression* rhs = expressionStack.front();
                        expressionStack.pop_front();
                        Expression* lhs = expressionStack.front();
                        expressionStack.pop_front();
                        Expression* subExpression = new Plus( *lhs, *rhs );
                        expressionStack.push_front(subExpression);

                        m_Expressions.push_back(subExpression);
                }
                else if( (*it) == "-" )
                {
                        Expression* rhs = expressionStack.front();
                        expressionStack.pop_front();
                        Expression* lhs = expressionStack.front();
                        expressionStack.pop_front();
                        Expression* subExpression = new Minus( *lhs, *rhs );
                        expressionStack.push_front(subExpression);

                        m_Expressions.push_back(subExpression);
                }
                else
                {
                        Expression* subExpression = new Variable(*it);
                        expressionStack.push_front(subExpression);

                        m_Expressions.push_back(subExpression);
                }
        }

        m_SyntaxTree = expressionStack.front();
        expressionStack.pop_front();
}
vector< string > Evaluator::tokenize(const string& aExpression)
{
        istringstream istringStream(aExpression);
        vector< string > tokens;
        copy(istream_iterator< string >( istringStream ),
             istream_iterator< string >(),
             back_inserter< vector< string > >(tokens));
        return tokens;
}

Evaluator::~Evaluator()
{
        while(m_Expressions.empty() == false )
        {
                delete m_Expressions.back();
                m_Expressions.pop_back();
        }
}

int Evaluator::interpret(map< string, Expression* > aContext)
{
        return m_SyntaxTree->interpret(aContext);
}

