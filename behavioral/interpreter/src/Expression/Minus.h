#ifndef __MINUS_H__
#define __MINUS_H__ 1

#include "Expression/Expression.h"

class Minus : public Expression
{
public:
    Minus(Expression& left, Expression& right);
    virtual ~Minus();

public:
    virtual int interpret(std::map< std::string, Expression* > variables);

private:
    Expression& m_Left;
    Expression& m_Right;

};

#endif
