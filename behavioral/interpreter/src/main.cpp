#include "Expression/Evaluator.h"
#include "Expression/Number.h"

#include <iostream>

int main()
{
    std::string expression("w x z - +");
    Evaluator sentence(expression);
    std::map< std::string, Expression* > variables;
    variables["w"] = new Number(5);
    variables["x"] = new Number(10);
    variables["z"] = new Number(42);
    int result = sentence.interpret(variables);
    std::cout << result << std::endl;
    return 0;
}
