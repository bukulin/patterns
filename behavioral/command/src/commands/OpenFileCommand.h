/** OpenFileCommand.h ---
 *
 * Copyright (C) 2012 Bukuli Norbert
 *
 * Author: Bukuli Norbert <bukulin@szoft-nbukuli3.local.mediso.hu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */


#ifndef OpenFileCommand_h
#define OpenFileCommand_h 1

#include "commands/CommandListener.h"

class Application;

class OpenFileCommand : public CommandListener
{
public:
	OpenFileCommand( Application* application );
	virtual ~OpenFileCommand();

public:
	virtual void Execute();

private:
	Application* application;
};
#endif // OpenFileCommand_h
