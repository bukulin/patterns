#include "Loggers/Logger.h"
#include "Loggers/StdoutLogger.h"
#include "Loggers/EmailLogger.h"
#include "Loggers/StderrLogger.h"

static Logger* createChain(void)
{
	Logger* logger = new StdoutLogger(Logger::Debug);
	Logger* logger1 = new EmailLogger(Logger::Notice);
	Logger* logger2 = new StderrLogger(Logger::Error);

	logger->setNext( logger1 );
	logger1->setNext( logger2 );

	return logger;
}

int main(void)
{
	Logger* logger = createChain();

	logger->message( "Entering function \"f\"\n", Logger::Debug);
	logger->message( "Step1 completed.\n", Logger::Notice);
	logger->message( "An error has occurred.\n", Logger::Error );

	delete logger;

	return 0;
}
