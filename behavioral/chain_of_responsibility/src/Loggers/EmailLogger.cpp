#include "EmailLogger.h"

#include <iostream>

EmailLogger::EmailLogger( const int mask )
	: Logger( mask )
{
}

void EmailLogger::writeMessage( const std::string& msg )
{
	std::cout << "Sendig via e-mail: " << msg;
}
