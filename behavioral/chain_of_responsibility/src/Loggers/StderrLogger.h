// -*- mode: c++ -*-
#ifndef StderrLogger_h
#define StderrLogger_h 1

#include "Logger.h"

class StderrLogger : public Logger
{
public:
	StderrLogger( const int mask );

public:
	void writeMessage( const std::string& msg );
};

#endif // StderrLogger_h
