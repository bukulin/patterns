#include "StderrLogger.h"

#include <iostream>

StderrLogger::StderrLogger( const int mask )
	: Logger( mask )
{
}

void StderrLogger::writeMessage( const std::string& msg )
{
	std::cout << "Sendig to stderr: " << msg;
}
