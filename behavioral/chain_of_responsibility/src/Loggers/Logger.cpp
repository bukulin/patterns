#include "Logger.h"

Logger::Logger( const int aMask )
	: mask( aMask )
	, next( NULL )
{
}

Logger::~Logger()
{
}

void Logger::setNext( Logger* logger )
{
	next = logger;
}

void Logger::message( const std::string& msg,
		      const int priority )
{
	if(priority <= mask)
		writeMessage( msg );
	if(next != NULL)
		next->message( msg, priority );
}
