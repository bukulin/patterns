// -*- mode: c++ -*-
#ifndef StdoutLogger_h
#define StdoutLogger_h 1

#include "Logger.h"

class StdoutLogger : public Logger
{
public:
	StdoutLogger(const int mask);

public:
	void writeMessage( const std::string& msg );
};

#endif // StdoutLogger_h
