// -*- mode: c++ -*-
#ifndef Logger_h
#define Logger_h 1

#include <string>

class Logger
{
public:
	enum Level
	{
		Error = 3,
		Notice = 5,
		Debug = 7
	};

protected:
	Logger( const int mask );
public:
	virtual ~Logger();

public:
	void setNext( Logger* logger );
	void message( const std::string& msg,
		      const int priority );

protected:
	virtual void writeMessage( const std::string& msg ) = 0;

protected:
	Logger* next;

private:
	const int mask;
};

#endif // Logger_h
