#include "StdoutLogger.h"

#include <iostream>

StdoutLogger::StdoutLogger( const int mask )
	: Logger(mask)
{}

void StdoutLogger::writeMessage( const std::string& msg )
{
	std::cout << "Writing to stdout: " << msg ;
}
