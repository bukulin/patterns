// -*- mode: c++ -*-
#ifndef EmailLogger_h
#define EmailLogger_h 1

#include "Logger.h"

class EmailLogger : public Logger
{
public:
	EmailLogger( const int mask );

public:
	void writeMessage( const std::string& msg );
};

#endif // EmailLogger_h
