#include <Fruit/Fruit.h>
#include <iostream>

std::map< std::string, Fruit* > Fruit::types;

Fruit::~Fruit()
{}

Fruit* Fruit::getFruit( const std::string& aType )
{
	std::map< std::string, Fruit* >::iterator it = types.find( aType );

	Fruit *fruit = NULL;
	if( it == types.end() )
	{
		fruit = new Fruit( aType );
		types[ aType ] = fruit;
	}
	else
	{
		fruit = (*it).second;
	}

	return fruit;
}

void Fruit::clearContent()
{
	for( std::map< std::string, Fruit* >::const_iterator it = types.begin();
		 it != types.end();
		 ++it )
	{
		delete (*it).second;
	}

	types.clear();
}

void Fruit::printCurrentTypes()
{
	std::cout << "Number of instances: " << types.size() << std::endl
			  << "Curent types:" << std::endl;
	for( std::map< std::string, Fruit* >::const_iterator it = types.begin();
		 it != types.end();
		 ++it )
	{
		std::cout << "\t" << (*it).first << std::endl;
	}
	std::cout << "----" << std::endl;
}

Fruit::Fruit( const std::string& aType )
	: type( aType )
{}
