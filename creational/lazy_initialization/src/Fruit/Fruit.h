#ifndef _FRUIT_H
#define _FRUIT_H

#include <string>
#include <map>

class Fruit
{
public:
	~Fruit();
	static Fruit* getFruit( const std::string& type );
	static void clearContent();
	static void printCurrentTypes();

private:
	Fruit( const std::string& type );

private:
	static std::map< std::string, Fruit* > types;
	std::string type;
};

#endif
