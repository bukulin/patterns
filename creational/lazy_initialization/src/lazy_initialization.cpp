#include <Fruit/Fruit.h>

int main()
{
	Fruit::getFruit( "Banana" );
	Fruit::printCurrentTypes();

	Fruit::getFruit( "Apple" );
	Fruit::printCurrentTypes();

	Fruit::getFruit( "Banana" );
	Fruit::printCurrentTypes();

	Fruit::clearContent();

	return 0;
}
