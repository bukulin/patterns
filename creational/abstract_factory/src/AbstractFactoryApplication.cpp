#include <iostream>
#include <stdexcept>
#include <products/Button/Button.h>
#include <products/Window/Window.h>
#include <factories/WinFactory.h>
#include <factories/OSXFactory.h>
#include <AbstractFactoryApplication.h>

AbstractFactoryApplication::AbstractFactoryApplication( GUIFactory* aFactory )
	: button( NULL )
	, window( NULL )
	, factory( aFactory )
{
	if (aFactory == NULL)
	{
		return;
	}

	button = factory->createButton();
	button->paint();

	window = factory->createWindow();
	window->paint();
}

AbstractFactoryApplication::~AbstractFactoryApplication()
{
	delete button;
	delete factory;
}

GUIFactory* AbstractFactoryApplication::createOSSpecificFactory()
{
	unsigned int sys = 0xff;

	std::cout << std::endl
			  << "Enter OS type (0 - Win, 1 - OSX): ";
	std::cin >> sys;

	switch (sys)
	{
	case 0:
		return new WinFactory();

	case 1:
		return new OSXFactory();

	default:
		throw std::runtime_error("Bad OS type");
	}
}

