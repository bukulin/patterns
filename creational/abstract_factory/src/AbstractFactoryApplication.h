#ifndef _APPLICATION_H
#define _APPLICATION_H

class GUIFactory;
class Button;
class Window;

class AbstractFactoryApplication
{
public:
	AbstractFactoryApplication( GUIFactory* factory );
	~AbstractFactoryApplication();

public:
	static GUIFactory* createOSSpecificFactory();

private:
	AbstractFactoryApplication( const AbstractFactoryApplication& );
	AbstractFactoryApplication& operator=( const AbstractFactoryApplication& );

private:
	Button* button;
	Window* window;
	GUIFactory* factory;
};

#endif
