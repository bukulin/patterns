#ifndef _OSXWINDOW_H
#define _OSXWINDOW_H

#include <products/Window/Window.h>

class OSXWindow : public Window
{
public:
	OSXWindow();
	virtual ~OSXWindow();

	virtual void paint();
};

#endif
