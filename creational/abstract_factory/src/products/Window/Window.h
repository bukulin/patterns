#ifndef _WINDOW_H
#define _WINDOW_H

class Window
{
public:
	Window();
	virtual ~Window();

	virtual void paint() = 0;
};

#endif
