#ifndef _WINWINDOW_H
#define _WINWINDOW_H

#include <products/Window/Window.h>

class WinWindow : public Window
{
public:
	WinWindow();
	virtual ~WinWindow();

	virtual void paint();
};

#endif
