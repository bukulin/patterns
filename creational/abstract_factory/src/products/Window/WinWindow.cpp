#include <products/Window/WinWindow.h>

#include <iostream>

WinWindow::WinWindow()
	: Window()
{}

WinWindow::~WinWindow()
{}

void WinWindow::paint()
{
	std::cout << "I'm a WinWindow\n";
}
