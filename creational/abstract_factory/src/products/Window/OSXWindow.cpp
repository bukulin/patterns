#include <products/Window/OSXWindow.h>

#include <iostream>

OSXWindow::OSXWindow()
	: Window()
{}

OSXWindow::~OSXWindow()
{}

void OSXWindow::paint()
{
	std::cout << "I'm an OSXWindow\n";
}
