#include <products/Button/WinButton.h>

#include <iostream>

WinButton::WinButton()
	: Button()
{}

WinButton::~WinButton()
{}

void WinButton::paint()
{
	std::cout << "I'm a WinButton\n";
}
