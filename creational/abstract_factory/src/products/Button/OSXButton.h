#ifndef _OSXBUTTON_H
#define _OSXBUTTON_H

#include <products/Button/Button.h>

class OSXButton: public Button
{
public:
	OSXButton();
	virtual ~OSXButton();
	virtual void paint();
};

#endif
