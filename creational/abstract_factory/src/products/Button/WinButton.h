#ifndef _WINBUTTON_H
#define _WINBUTTON_H

#include <products/Button/Button.h>

class WinButton: public Button
{
public:
	WinButton();
	virtual ~WinButton();
	virtual void paint();
};

#endif
