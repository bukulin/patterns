#ifndef _BUTTON_H
#define _BUTTON_H

class Button
{
public:
	Button();
	virtual ~Button();

	virtual void paint() = 0;
};

#endif
