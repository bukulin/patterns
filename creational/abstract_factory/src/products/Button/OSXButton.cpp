#include <products/Button/OSXButton.h>

#include <iostream>

OSXButton::OSXButton()
	: Button()
{}

OSXButton::~OSXButton()
{}

void OSXButton::paint()
{
	std::cout << "I'm an OSXButton\n";
}
