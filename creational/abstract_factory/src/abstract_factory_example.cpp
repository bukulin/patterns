#include <iostream>
#include <exception>
#include <AbstractFactoryApplication.h>


void doApplication()
{
	GUIFactory* factory = AbstractFactoryApplication::createOSSpecificFactory();
	AbstractFactoryApplication* application = new AbstractFactoryApplication( factory );
	delete application;
}

void handleException( const std::exception& e )
{
	std::cout << e.what() << std::endl;
}

int main()
{
	try
	{
		doApplication();
	}
	catch(const std::exception& e)
	{
		handleException( e );
	}

	return 0;
}
