#ifndef _GUIFACTORY_H
#define _GUIFACTORY_H

class Button;
class Window;

class GUIFactory
{
public:
	GUIFactory();
	virtual ~GUIFactory();

	virtual Button* createButton() = 0;
	virtual Window* createWindow() = 0;
};

#endif
