#include <products/Button/OSXButton.h>
#include <products/Window/OSXWindow.h>
#include <factories/OSXFactory.h>

OSXFactory::OSXFactory()
	: GUIFactory()
{}

OSXFactory::~OSXFactory()
{}

Button* OSXFactory::createButton()
{
	return new OSXButton();
}

Window* OSXFactory::createWindow()
{
	return new OSXWindow();
}
