#ifndef _OSXFACTORY_H
#define _OSXFACTORY_H

#include <factories/GUIFactory.h>

class OSXFactory : public GUIFactory
{
public:
	OSXFactory();
	~OSXFactory();

	Button* createButton();
	Window* createWindow();
};

#endif
