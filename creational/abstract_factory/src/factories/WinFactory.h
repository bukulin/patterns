#ifndef _WINFACTORY_H
#define _WINFACTORY_H

#include <factories/GUIFactory.h>

class WinFactory : public GUIFactory
{
public:
	WinFactory();
	~WinFactory();

	Button* createButton();
	Window* createWindow();
};

#endif
