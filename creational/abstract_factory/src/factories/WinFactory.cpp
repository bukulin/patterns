#include <products/Button/WinButton.h>
#include <products/Window/WinWindow.h>
#include <factories/WinFactory.h>

WinFactory::WinFactory()
	: GUIFactory()
{}

WinFactory::~WinFactory()
{}

Button* WinFactory::createButton()
{
	return new WinButton();
}

Window* WinFactory::createWindow()
{
	return new WinWindow();
}
