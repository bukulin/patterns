#include <document/Document.h>
#include <iostream>

Document::Document( const std::string& aName )
 : name( aName )
{
	std::cout << "\t" << __PRETTY_FUNCTION__ << std::endl;
}
Document::~Document()
{
	std::cout << "\t" << __PRETTY_FUNCTION__ << std::endl;
}

const std::string& Document::getName() const
{
	return name;
}
