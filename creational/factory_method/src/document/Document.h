#ifndef _DOCUMENT_H
#define _DOCUMENT_H

#include <string>

class Document
{
public:
	Document( const std::string& name );
	virtual ~Document();

	virtual void open() = 0;
	virtual void close() = 0;

	const std::string& getName() const;

private:
	std::string name;
};

#endif
