#include <document/WinDocument.h>
#include <iostream>

WinDocument::WinDocument( const std::string& aName )
	: Document( aName )
{
	std::cout << "\t" << __PRETTY_FUNCTION__ << std::endl;
}

WinDocument::~WinDocument()
{
	std::cout << "\t" << __PRETTY_FUNCTION__ << std::endl;
}

void WinDocument::open()
{
	std::cout << "\t" << __PRETTY_FUNCTION__ << std::endl;
}
void WinDocument::close()
{
	std::cout << "\t" << __PRETTY_FUNCTION__ << std::endl;
}

