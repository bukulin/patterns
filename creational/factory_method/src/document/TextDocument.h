#ifndef _TEXTDOCUMENT_H
#define _TEXTDOCUMENT_H

#include <document/Document.h>

class TextDocument: public Document
{
public:
	TextDocument( const std::string& name );
	virtual ~TextDocument();

	virtual void open();
	virtual void close();
};

#endif
