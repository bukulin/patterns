#include <document/TextDocument.h>
#include <iostream>

TextDocument::TextDocument( const std::string& aName )
	: Document( aName )
{
	std::cout << "\t" << __PRETTY_FUNCTION__ << std::endl;
}

TextDocument::~TextDocument()
{
	std::cout << "\t" << __PRETTY_FUNCTION__ << std::endl;
}

void TextDocument::open()
{
	std::cout << "\t" << __PRETTY_FUNCTION__ << std::endl;
}
void TextDocument::close()
{
	std::cout << "\t" << __PRETTY_FUNCTION__ << std::endl;
}

