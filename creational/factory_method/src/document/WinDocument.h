#ifndef _WINDOCUMENT_H
#define _WINDOCUMENT_H

#include <document/Document.h>

class WinDocument: public Document
{
public:
	WinDocument( const std::string& name );
	virtual ~WinDocument();

	virtual void open();
	virtual void close();
};

#endif
