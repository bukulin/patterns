#ifndef _WINAPPLICATION_H
#define _WINAPPLICATION_H

#include <application/Application.h>

class Document;

class WinApplication : public Application
{
public:
	WinApplication();
	~WinApplication();

	virtual Document* createDocument( const std::string& name );
};

#endif
