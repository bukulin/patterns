#ifndef _APPLICATION_H
#define _APPLICATION_H

#include <string>
#include <list>

class Document;

class Application
{
public:
	Application();
	virtual ~Application();

	void newDocument( const std::string& name );

	void openDocument();
	void reportDocuments() const;

	virtual Document* createDocument( const std::string& name ) = 0;

private:
	std::list< Document* > documents;
};

#endif
