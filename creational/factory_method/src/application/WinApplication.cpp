#include <application/WinApplication.h>
#include <document/WinDocument.h>
#include <iostream>

WinApplication::WinApplication()
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
}
WinApplication::~WinApplication()
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
}

Document* WinApplication::createDocument( const std::string& aName )
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;

	return new WinDocument( aName );
}
