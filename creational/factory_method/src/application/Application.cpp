#include <application/Application.h>
#include <document/Document.h>
#include <iostream>

Application::Application()
	: documents()
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
}
Application::~Application()
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	while ( documents.empty() == false )
	{
		Document* document = documents.back();
		documents.pop_back();
		delete document;
	}
}

void Application::newDocument( const std::string& aName )
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;

	Document* document = createDocument( aName );
	documents.push_back( document );
	document->open();
}

void Application::openDocument()
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
}

void Application::reportDocuments() const
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	for ( std::list< Document* >::const_iterator it = documents.begin();
		  it != documents.end();
		  ++it )
	{
		std::cout << "\t" << (*it)->getName() << std::endl;
	}
}
