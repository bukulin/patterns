#include <application/TextApplication.h>
#include <document/TextDocument.h>
#include <iostream>

TextApplication::TextApplication()
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
}
TextApplication::~TextApplication()
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
}

Document* TextApplication::createDocument( const std::string& aName )
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;

	return new TextDocument( aName );
}
