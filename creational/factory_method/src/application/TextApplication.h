#ifndef _TEXTAPPLICATION_H
#define _TEXTAPPLICATION_H

#include <application/Application.h>

class Document;

class TextApplication : public Application
{
public:
	TextApplication();
	~TextApplication();

	virtual Document* createDocument( const std::string& name );
};

#endif
