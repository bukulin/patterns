add_executable( factory_method_example
	factory_method_example.cpp
	FactoryMethodApplication.cpp
	document/Document.cpp
	document/TextDocument.cpp
	document/WinDocument.cpp
	application/Application.cpp
	application/TextApplication.cpp
	application/WinApplication.cpp )

include_directories( "${CMAKE_CURRENT_SOURCE_DIR}" )