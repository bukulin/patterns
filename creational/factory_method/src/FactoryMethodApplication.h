#ifndef _FACTORYMETHODAPPLICATION_H
#define _FACTORYMETHODAPPLICATION_H


class Application;

class FactoryMethodApplication
{
public:
	FactoryMethodApplication();
	~FactoryMethodApplication();

public:
	static Application* createSpecificApplication();

private:
	FactoryMethodApplication( const FactoryMethodApplication& );
	FactoryMethodApplication& operator=( const FactoryMethodApplication& );

private:
	Application* application;
};

#endif
