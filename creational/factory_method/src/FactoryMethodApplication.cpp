#include <FactoryMethodApplication.h>
#include <application/TextApplication.h>
#include <application/WinApplication.h>
#include <iostream>
#include <stdexcept>

FactoryMethodApplication::FactoryMethodApplication()
	: application( createSpecificApplication() )
{
	application->newDocument( "elso" );
	application->newDocument( "masodik" );
	application->reportDocuments();
}
FactoryMethodApplication::~FactoryMethodApplication()
{
	delete application;
}

Application* FactoryMethodApplication::createSpecificApplication()
{
	unsigned int kind = 0xff;

	std::cout << std::endl
			  << "Enter application type (0 - Text, 1 - Win): ";
	std::cin >> kind;

	switch (kind)
	{
	case 0:
		return new TextApplication();

	case 1:
		return new WinApplication();

	default:
		throw std::runtime_error("Bad applycation type");
	}
}
