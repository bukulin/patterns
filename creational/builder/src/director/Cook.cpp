#include <stdexcept>

#include <product/Pizza.h>
#include <builder/PizzaBuilder.h>
#include <director/Cook.h>

Cook::Cook()
	: pizzaBuilder( NULL )
{}
Cook::~Cook()
{}

void Cook::setPizzaBuilder( PizzaBuilder* aPizzaBuilder )
{
	pizzaBuilder = aPizzaBuilder;
}
const Pizza& Cook::getPizza()
{
	if (pizzaBuilder == NULL)
	{
		throw std::runtime_error("No pizza builder was given.");
	}
	return pizzaBuilder->getPizza();
}
void Cook::constructPizza()
{
	pizzaBuilder->buildDough();
	pizzaBuilder->buildSauce();
	pizzaBuilder->buildTopping();
}
