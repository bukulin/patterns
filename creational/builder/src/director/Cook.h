#ifndef _COOK_H
#define _COOK_H

class Pizza;
class PizzaBuilder;

class Cook
{
public:
	Cook();
	~Cook();

	void setPizzaBuilder( PizzaBuilder* pizzaBuilder );
	const Pizza& getPizza();
	void constructPizza();

private:
	Cook(const Cook&);
	Cook& operator=(const Cook&);

private:
	PizzaBuilder* pizzaBuilder;
};

#endif
