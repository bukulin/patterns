#include <product/Pizza.h>

#include <iostream>

Pizza::Pizza()
	: dough()
	, sauce()
	, topping()
{}
Pizza::~Pizza()
{}

void Pizza::setDough( const std::string& aDough )
{
	dough = aDough;
}
void Pizza::setSauce( const std::string& aSauce )
{
	sauce = aSauce;
}
void Pizza::setTopping( const std::string& aTopping )
{
	topping = aTopping;
}

void Pizza::open() const
{
	std::cout << "Pizza with "
			  << dough << " dough, "
			  << sauce << " sauce and "
			  << topping << " topping."
			  << std::endl;
}
