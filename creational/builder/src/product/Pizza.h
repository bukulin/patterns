#ifndef _PIZZA_H
#define _PIZZA_H

#include <string>

class Pizza
{
public:
	Pizza();
	~Pizza();

	void setDough( const std::string& dough );
	void setSauce( const std::string& sauce );
	void setTopping( const std::string& topping );
	void open() const;

private:
	std::string dough;
	std::string sauce;
	std::string topping;
};

#endif
