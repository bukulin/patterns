#include <iostream>
#include <stdexcept>
#include <builder/PizzaBuilder.h>
#include <builder/HawaiianPizzaBuilder.h>
#include <builder/SpicyPizzaBuilder.h>
#include <director/Cook.h>
#include <BuilderApplication.h>

BuilderApplication::BuilderApplication()
	: pizzaBuilder( createPizzaBuilder() )
{
	Cook cook;

	cook.setPizzaBuilder( pizzaBuilder );
	cook.constructPizza();
	Pizza pizza = cook.getPizza();
	pizza.open();
}
BuilderApplication::~BuilderApplication()
{
	delete pizzaBuilder;
}

PizzaBuilder* BuilderApplication::createPizzaBuilder()
{
	unsigned int kind = 0xff;

	std::cout << std::endl
			  << "What kind of pizza do you want to eat? (0 - Hawaiian, 1 - Spicy): ";
	std::cin >> kind;

	switch (kind)
	{
	case 0:
		return new HawaiianPizzaBuilder();

	case 1:
		return new SpicyPizzaBuilder();

	default:
		throw std::runtime_error("Bad sort of pizza");
	}
}
