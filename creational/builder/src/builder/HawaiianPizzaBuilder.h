#ifndef _HAWAIIANPIZZABUILDER_H
#define _HAWAIIANPIZZABUILDER_H

#include <builder/PizzaBuilder.h>

class HawaiianPizzaBuilder : public PizzaBuilder
{
public:
	HawaiianPizzaBuilder();
	virtual ~HawaiianPizzaBuilder();

	virtual void buildDough();
	virtual void buildSauce();
	virtual void buildTopping();
};

#endif
