#include <builder/PizzaBuilder.h>

PizzaBuilder::PizzaBuilder()
	: pizza()
{}

PizzaBuilder::~PizzaBuilder()
{}

const Pizza& PizzaBuilder::getPizza()
{
	return pizza;
}

