#ifndef _SPICYPIZZABUILDER_H
#define _SPICYPIZZABUILDER_H

#include <builder/PizzaBuilder.h>

class SpicyPizzaBuilder : public PizzaBuilder
{
public:
	SpicyPizzaBuilder();
	virtual ~SpicyPizzaBuilder();

	virtual void buildDough();
	virtual void buildSauce();
	virtual void buildTopping();
};

#endif
