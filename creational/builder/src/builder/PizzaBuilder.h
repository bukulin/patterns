#ifndef _PIZZABUILDER_H
#define _PIZZABUILDER_H

#include <product/Pizza.h>

class PizzaBuilder
{
public:
	PizzaBuilder();
	virtual ~PizzaBuilder();

	const Pizza& getPizza();

	virtual void buildDough() = 0;
	virtual void buildSauce() = 0;
	virtual void buildTopping() = 0;

protected:
	Pizza pizza;
};

#endif
