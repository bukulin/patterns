#include <builder/HawaiianPizzaBuilder.h>

HawaiianPizzaBuilder::HawaiianPizzaBuilder()
	: PizzaBuilder()
{}
HawaiianPizzaBuilder::~HawaiianPizzaBuilder()
{}

void HawaiianPizzaBuilder::buildDough()
{
	pizza.setDough("cross");
}
void HawaiianPizzaBuilder::buildSauce()
{
	pizza.setSauce("mild");
}
void HawaiianPizzaBuilder::buildTopping()
{
	pizza.setTopping("ham+pineapple");
}
