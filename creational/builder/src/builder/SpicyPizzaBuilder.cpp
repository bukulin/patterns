#include <builder/SpicyPizzaBuilder.h>

SpicyPizzaBuilder::SpicyPizzaBuilder()
	: PizzaBuilder()
{}
SpicyPizzaBuilder::~SpicyPizzaBuilder()
{}

void SpicyPizzaBuilder::buildDough()
{
	pizza.setDough("pan baked");
}
void SpicyPizzaBuilder::buildSauce()
{
	pizza.setSauce("hot");
}
void SpicyPizzaBuilder::buildTopping()
{
	pizza.setTopping("pepperoni+salami");
}
