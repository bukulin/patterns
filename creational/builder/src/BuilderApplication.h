#ifndef _BUILDERAPPLICATION_H
#define _BUILDERAPPLICATION_H

class PizzaBuilder;

class BuilderApplication
{
public:
	BuilderApplication();
	~BuilderApplication();

public:
	static PizzaBuilder* createPizzaBuilder();

private:
	BuilderApplication( const BuilderApplication& );
	BuilderApplication& operator=( const BuilderApplication& );

private:
	PizzaBuilder* pizzaBuilder;
};

#endif
