#include "ThreadPoolManager.h"

#include <iostream>

ThreadPoolManager* ThreadPoolManager::instance = nullptr;

ThreadPoolManager* ThreadPoolManager::GetInstance()
{
	if(instance == nullptr)
		instance = new ThreadPoolManager();
	return instance;
}
void ThreadPoolManager::ReleaseInstance()
{
	delete instance;
	instance = nullptr;
}

ThreadPoolManager::ThreadPoolManager()
	: numberOfThreads(0)
{
	std::cout << "[Manager] Ctor" << std::endl;
}
ThreadPoolManager::~ThreadPoolManager()
{
	std::cout << "[Manager] Dtor" << std::endl;
}

void ThreadPoolManager::NewThread()
{
	numberOfThreads += 1;
}
unsigned int ThreadPoolManager::NumberOfThreads() const
{
	return numberOfThreads;
}
