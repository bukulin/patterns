#include "ThreadPoolManager.h"

#include <iostream>

using namespace std;

int main(void)
{
	ThreadPoolManager* threadPoolManager =
		ThreadPoolManager::GetInstance();

	cout << "Number of threads: "
	     << threadPoolManager->NumberOfThreads()
	     << endl;
	threadPoolManager->NewThread();
	cout << "Number of threads: "
	     << threadPoolManager->NumberOfThreads()
	     << endl;

	ThreadPoolManager::ReleaseInstance();

	return 0;
}
