#ifndef ThreadPoolManager_h
#define ThreadPoolManager_h 1

class ThreadPoolManager final
{
private:
	ThreadPoolManager();
	~ThreadPoolManager();

public:
	static ThreadPoolManager* GetInstance();
	static void ReleaseInstance();

public:
	void NewThread();
	unsigned int NumberOfThreads() const;

private:
	static ThreadPoolManager* instance;

private:
	unsigned int numberOfThreads;
};

#endif
