#ifndef Cyborg_h
#define Cyborg_h 1

#include "Monster.h"

class Cyborg : public Monster
{
public:
	Cyborg();
	~Cyborg();

public:
	void Attack() override;
	void LoadSounds() override;
	void LoadTextures() override;

	Monster* Clone() override;
};

#endif
