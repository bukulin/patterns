#include "MonsterRegistry.h"

#include "Acid.h"
#include "Alien.h"
#include "Cyborg.h"

Monster* MonsterRegistry::CreateMonster(const std::string& type)
{
	Monster* monster = NULL;

	if(type == "Acid")
		monster = new Acid;
	else if(type == "Alien")
		monster = new Alien;
	else if(type == "Cyborg")
		monster = new Cyborg;

	monster->LoadSounds();
	monster->LoadTextures();

	return monster;
}
