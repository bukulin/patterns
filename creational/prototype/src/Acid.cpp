#include "Acid.h"

#include <iostream>

Acid::Acid()
	: Monster()
{}

Acid::~Acid()
{}

void Acid::Attack()
{
	std::cout << "[Acid] Attacking" << std::endl;
}

void Acid::LoadSounds()
{
	std::cout << "[Acid] Loading sounds" << std::endl;
}

void Acid::LoadTextures()
{
	std::cout << "[Acid] Loading textures" << std::endl;
}

Monster* Acid::Clone()
{
	// The default copy constructor will return a shallow copy
	return new Acid(*this);
}
