#ifndef Alien_h
#define Alien_h 1

#include "Monster.h"

class Alien : public Monster
{
public:
	Alien();
	~Alien();

public:
	void Attack() override;
	void LoadSounds() override;
	void LoadTextures() override;

	Monster* Clone() override;
};

#endif
