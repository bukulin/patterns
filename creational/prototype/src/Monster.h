#ifndef Monster_h
#define Monster_h 1

class Monster
{
protected:
	Monster();
public:
	virtual ~Monster();

public:
	virtual void Attack() = 0;
	virtual void LoadSounds() = 0;
	virtual void LoadTextures() = 0;

	virtual Monster* Clone() = 0;
};

#endif
