#ifndef MonsterRegistry_h
#define MonsterRegistry_h 1

#include "MonsterTypeHelper.h"

#include <string>

class MonsterRegistry
{
public:
	Monster* CreateMonster(const std::string& type);
};

#endif
