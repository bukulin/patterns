#include "Cyborg.h"

#include <iostream>

Cyborg::Cyborg()
	: Monster()
{}

Cyborg::~Cyborg()
{}

void Cyborg::Attack()
{
	std::cout << "[Cyborg] Attacking" << std::endl;
}

void Cyborg::LoadSounds()
{
	std::cout << "[Cyborg] Loading sounds" << std::endl;
}

void Cyborg::LoadTextures()
{
	std::cout << "[Cyborg] Loading textures" << std::endl;
}

Monster* Cyborg::Clone()
{
	// The default copy constructor will return a shallow copy
	return new Cyborg(*this);
}
