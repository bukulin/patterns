#include "MonsterRegistry.h"
#include "MonsterManager.h"

#include "Monster.h"
#include "Alien.h"
#include "Cyborg.h"
#include "Acid.h"

#include <iostream>

static void UsingPrototypeManager(MonsterManager& monsterManager)
{
	// Initializing the prototype manager
	monsterManager.RegisterMonster("Alien", new Alien());
	monsterManager.RegisterMonster("Cyborg", new Cyborg());
	monsterManager.RegisterMonster("Acid", new Acid());

	// This shall not do a thing, because Cyborg is already registered
	monsterManager.RegisterMonster("Cyborg", new Cyborg());

	std::cout << "Getting instance of Monsters from MonsterManager" << std::endl;
	MonsterPtr cyborg1( monsterManager.GetMonster("Cyborg") );
	cyborg1->Attack();
	MonsterPtr alien1( monsterManager.GetMonster("Alien") );
	alien1->Attack();
	MonsterPtr acid1( monsterManager.GetMonster("Acid") );
	acid1->Attack();

	std::cout << "Cloning the monsters..." << std::endl;
	MonsterPtr cyborg2(cyborg1->Clone());
	cyborg2->Attack();
	MonsterPtr alien2(alien1->Clone());
	alien2->Attack();
	MonsterPtr acid2(acid1->Clone());
	acid2->Attack();

}

static void UsingPrototype(MonsterRegistry& monsterRegistry)
{
	MonsterPtr cyborg1(monsterRegistry.CreateMonster("Cyborg"));
	cyborg1->Attack();
	MonsterPtr alien1(monsterRegistry.CreateMonster("Alien"));
	alien1->Attack();
	MonsterPtr acid1(monsterRegistry.CreateMonster("Acid"));
	acid1->Attack();

	std::cout << "Cloning the Monsters..." << std::endl;
	MonsterPtr cyborg2(cyborg1->Clone());
	cyborg2->Attack();
	MonsterPtr alien2(alien1->Clone());
	alien2->Attack();
	MonsterPtr acid2(acid1->Clone());
	acid2->Attack();
}

int main()
{
	std::cout << " -- With registry --" << std::endl;
	MonsterRegistry monsterRegistry;
	UsingPrototype(monsterRegistry);

	std::cout << std::endl << " -- With manager --" << std::endl;
	MonsterManager monsterManager;
	UsingPrototypeManager(monsterManager);
	return 0;
}
