#include "Alien.h"

#include <iostream>

Alien::Alien()
	: Monster()
{}

Alien::~Alien()
{}

void Alien::Attack()
{
	std::cout << "[Alien] Attacking" << std::endl;
}

void Alien::LoadSounds()
{
	std::cout << "[Alien] Loading sounds" << std::endl;
}

void Alien::LoadTextures()
{
	std::cout << "[Alien] Loading textures" << std::endl;
}

Monster* Alien::Clone()
{
	// The default copy constructor will return a shallow copy
	return new Alien(*this);
}
