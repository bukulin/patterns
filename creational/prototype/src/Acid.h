#ifndef Acid_h
#define Acid_h 1

#include "Monster.h"

class Acid : public Monster
{
public:
	Acid();
	~Acid();

public:
	void Attack() override;
	void LoadSounds() override;
	void LoadTextures() override;

	Monster* Clone() override;
};

#endif
