#include "MonsterManager.h"

#include "Monster.h"

#include <stdexcept>

bool MonsterManager::monsterAlreadyRegistered(const std::string& type) const
{
	return ( registry.find(type) != registry.end() );
}

void MonsterManager::RegisterMonster(const std::string& type, Monster* monster)
{
	if( monsterAlreadyRegistered(type) )
		return;

	monster->LoadSounds();
	monster->LoadTextures();
	MonsterPtr ptr(monster);
	registry[type] = ptr;
}

MonsterPtr MonsterManager::GetMonster(const std::string& type)
{
	if( monsterAlreadyRegistered(type) )
		return registry.find(type)->second;
	throw std::runtime_error("Monster not registered");
}
