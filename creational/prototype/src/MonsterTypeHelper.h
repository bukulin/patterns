#ifndef MonsterTypeHelper_h
#define MonsterTypeHelper_h 1

#include <memory>

class Monster;
typedef std::shared_ptr<Monster> MonsterPtr;

#endif
