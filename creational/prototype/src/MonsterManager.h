#ifndef MonsterManager_h
#define MonsterManager_h 1

#include "MonsterTypeHelper.h"

#include <string>
#include <map>

class Monster;

class MonsterManager
{
public:
	void RegisterMonster(const std::string& type, Monster* monster);
	MonsterPtr GetMonster(const std::string& type);

private:
	bool monsterAlreadyRegistered(const std::string& type) const;

private:
	typedef std::map<std::string, MonsterPtr> Monsters;
	Monsters registry;
};

#endif
